#!/bin/bash
function build() {
	rm -rf ./build
	mkdir build
	cd build
	cmake ..
}
function buildgr() {
	rm -rf ./build
	mkdir build
	cd build
	cmake -DPYTHON_EXECUTABLE=`which python` ../
}
function grmake() {
	cmake -DPYTHON_EXECUTABLE=`which python` ../
}
