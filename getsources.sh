git clone https://github.com/EttusResearch/uhd.git
git clone --recurse-submodules https://github.com/gnuradio/gnuradio.git
cd gnuradio
git checkout v3.7.13.4
cd volk
git submodule update --init --recursive
