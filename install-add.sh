#!/bin/bash

# has errors libdpdk-dev 
sudo apt install python-requests libgps-dev python3-mako liborc-0.4-dev
sudo apt install \
 libgsm1 \
 libgts-0.7-5 \
 libgts-bin \
 libgvc6 \
 libgvpr2 \
 libjack-jackd2-0 \
 libjs-jquery-ui \
 liblog4cpp5-dev \
 liblog4cpp5v5 \
 libopus0 \
 libpathplan4 \
 libportaudio2 \
 libqwt5-qt4 \
 librtlsdr0 \
 libsamplerate0 \
 libtcl8.6 \
 libtk8.6 \
 libuhd003 \
 libvolk1-bin \
 libvolk1-dev \
 libvolk1.3 \
 libxaw7 \
 libxdot4 \
 libxft2 \
 libxmu6 \
 libxpm4 \
 libxss1 \
 python-cycler \
 python-dateutil \
 python-decorator \
 python-functools32 \
 python-imaging \
 python-matplotlib \
 python-matplotlib-data \
 python-networkx \
 python-opengl \
 python-pygraphviz \
 python-pyparsing \
 python-qwt5-qt4 \
 python-scipy \
 python-subprocess32 \
 python-tk \
 python-zmq \
 rtl-sdr \
 tk8.6-blt2.5 \
 ttf-bitstream-vera \
 python-gi \
 python-gi-cairo \
 python3-gi \
 python3-gi-cairo \
 gir1.2-gtk-3.0 \
 libqwt-qt5-dev \
 python-gmpy2 \
 libgmp-dev \
 libqwt-headers \
 libqwt-qt5-6 \
 thrift-compiler \
 libqwt-dev \
 libqwt-qt5-dev \
 python-pyqt5
# libqwt5-qt4-dev trying
# Suggested 
#blt-demo
#gsfonts
#graphviz-doc
#libgd-tools
#jackd2
#libjs-jquery-ui-docs
#opus-tools
#tcl8.6
#tk8.6
#python-cycler-doc
#dvipng 
#ffmpeg 
#gir1.2-gtk-3.0 
#ghostscript
#inkscape 
#ipython 
#python-cairocffi
#python-configobj
#python-excelerator
#python-gobject 
#python-matplotlib-doc 
#python-nose 
#python-tornado 
#python-traits 
#texlive-extra-utils
#texlive-latex-extra 
#ttf-staypuft 
#python-networkx-doc 
#libgle3 
#python-pygraphviz-doc 
#python-pyparsing-doc 
#libqwt5-qt4-dev 
#python-scipy-doc 
#tix 
#python-tk-dbg
